package ta;

public class Main extends Bayes{
    private String PDuration;
    private float CreditGood = 0 , CreditBad = 0 ;
    private String PAge;
    private String PJob;

    protected void input() {
        String query = "Select count(*) from data where ";
        if(Integer.parseInt(PDuration) <= 24) query += "duration <= 24 && ";
        else query += "duration > 24 && ";
        
        if(Integer.parseInt(PAge) <= 40) query += "age <= 40 && ";
        else query += "age > 40 && ";
        
        query += " job = '"+PJob+"'";
        calculate(query);
    }

    private void calculate(String query) {
        float probGood = super.getData(query + " && credit = 'good'")/super.total;
        float probBad = super.getData(query + " && credit = 'bad'")/super.total;
        System.out.println("probGood = " + probGood);
        System.out.println("probBad = " + probBad);

        this.CreditGood = (probGood * super.good) / pembagi;
        this.CreditBad = (probBad * super.bad) / pembagi;
    }
    
    protected void setPDuration(String PDuration) {
        this.PDuration = PDuration;
    }

    protected void setPAge(String PAge) {
        this.PAge = PAge;
    }

    protected void setPJob(String PJob) {
        this.PJob = PJob;
    }
    
    protected float getCreditGood(){
        return this.CreditGood;
    }

    protected float getCreditBad(){
        return this.CreditBad;
    }
}
