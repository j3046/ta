package ta;

import java.awt.event.*;
import javax.swing.*;

public class GUI extends Main implements Interface{
    private JFrame frame;
    private JLabel credit, good, bad;
    
    public GUI() {
        frame = new JFrame();
        this.input();
    }
    
    @Override
    public void input(){
        frame.setSize(500,500);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setTitle("TRY TEXT FIELD");

        JLabel durL = new JLabel("Duration (month) : ");
        durL.setBounds(25, 25, 150, 25);
        frame.add(durL);

        JTextField durT = new JTextField();
        durT.setBounds(150, 25, 300, 25);
        frame.add(durT);

        JLabel ageL = new JLabel("Age : ");
        ageL.setBounds(25, 75, 150, 25);
        frame.add(ageL);

        JTextField ageT = new JTextField();
        ageT.setBounds(150, 75, 300, 25);
        frame.add(ageT);
        
        JLabel jobL = new JLabel("Job : ");
        jobL.setBounds(25, 125, 150, 25);
        frame.add(jobL);

        JComboBox jobT = new javax.swing.JComboBox<>();
        jobT.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "unskilled - non resident", "unskilled - resident", "skilled - employee",  "manager / self-employee / highly qualify employee" }));
        jobT.setBounds(150, 125, 300, 25);
        frame.add(jobT);

        JButton btnTampil = new JButton("Get Probability");
        frame.add(btnTampil);
        btnTampil.setBounds(150, 175, 150, 25);

        JLabel res = new JLabel("Result ");
        res.setBounds(25, 240, 150, 25);
        frame.add(res);
        
        JLabel credit = new JLabel("Credit : ");
        credit.setBounds(25, 275, 150, 25);
        frame.add(credit);
        
        JLabel good = new JLabel("Good Probability : ");
        good.setBounds(25, 300, 150, 25);
        frame.add(good);
        
        JLabel bad = new JLabel("Bad Probability : ");
        bad.setBounds(25, 325, 150, 25);
        frame.add(bad);
        
        this.credit = new JLabel("");
        this.credit.setBounds(150, 275, 150, 25);
        frame.add(this.credit);
        
        this.good = new JLabel("");
        this.good.setBounds(150, 300, 150, 25);
        frame.add(this.good);
        
        this.bad = new JLabel("");
        this.bad.setBounds(150, 325, 150, 25);
        frame.add(this.bad);
        
        frame.setLayout(null);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        btnTampil.addActionListener((e) -> {
            super.setPDuration(durT.getText());
            super.setPAge(ageT.getText());
            super.setPJob(jobT.getSelectedItem().toString());
            super.input();
            calculate();
        }); 
    }
    
    private void calculate() {
        if(super.getCreditGood() >= super.getCreditBad()) this.credit.setText("Good");
        else this.credit.setText("Bad");
            
        this.good.setText(super.getCreditGood() + "");
        this.bad.setText(super.getCreditBad() + "");
    }
}
