package ta;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Bayes{
    private Statement stmt;
    private String[] duration = {"<= 24", "> 24"};
    private String[] age = {"<= 40", "> 40"};
    private String[] job = {"unskilled - non resident", "unskilled - resident", "skilled - employee",  "manager / self-employee / highly qualify employee"};
    protected float total = 0, good=0, bad=0, pembagi=0;
    
    protected Bayes() {
        try {
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/bayes", "root", "");
            stmt = con.createStatement();
        } catch (SQLException e) {
            System.out.println("error DB = " + e);
        }
        
        initProbability();
    }
        
    protected float getData(String query){
        float res = 0;
        try{
            ResultSet rs = stmt.executeQuery(query);
            if (rs != null) {
                rs.next();
                res = Integer.parseInt(rs.getString(1));
            }
        }catch(SQLException e){
            System.out.println("error db getData= " + e);
        }
        return res;
    }
    
    private void initProbability(){
        total = getData("SELECT COUNT(*) FROM data");
        good = getData("SELECT COUNT(*) FROM data WHERE credit = 'good'") /total;
        bad = getData("SELECT COUNT(*) FROM data WHERE credit = 'bad'") /total;
        
        for (String duration1 : duration) {
            for (String age1 : age) {
                for (String job1 : job) {
                    pembagi += (getData("Select COUNT(*) FROM data WHERE duration " + duration1 + " && age " + age1 + " && job = '" + job1 + "' && credit = 'good'") / total * good) ;
                    pembagi += (getData("Select COUNT(*) FROM data WHERE duration " + duration1 + " && age " + age1 + " && job = '" + job1 + "' && credit = 'bad'") / total * bad) ;
                }
            }
        }
        System.out.println("total = " + total);
        System.out.println("pembagi = " + pembagi);
        System.out.println("good = " + good);
        System.out.println("bad = " + bad);
    } 
}